package com.codegreenllc.dashboard.demo;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Movie {
	private String director;
	private Long id;
	private String title;
}
package com.codegreenllc.dashboard.demo;

import java.util.Arrays;
import java.util.List;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
@RequestMapping("/movies")
public class MovieServiceApplication {

	public static void main(final String[] args) {
		SpringApplication.run(MovieServiceApplication.class, args);
	}

	private final List<Movie> movieList = Arrays.asList(
			Movie.builder().id(1L).title("Watchmen").director("Zack Snyder").build(),
			Movie.builder().id(2L).title("The Color of Magic").director("Vadim Jean").build());

	@GetMapping("")
	public List<Movie> findAllBooks() {
		return movieList;
	}

	@GetMapping("/{movieId}")
	public Movie findBook(@PathVariable final Long movieId) {
		return movieList.stream().filter(b -> b.getId().equals(movieId)).findFirst().orElse(null);
	}

}
